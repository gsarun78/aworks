package com.appbiz.arun.learningapp;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivityMarketArea extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    //private DBHelper mydb ;
    private DatePicker datePicker;
    Cursor c = null;

    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;
    public String item;
    public StringBuilder date;
    public CustomListViewRate customListViewRate;
    public ListView lst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        setContentView(R.layout.activity_main_market_area);

        String value = getIntent().getStringExtra("TEXT");
        Log.d("Message", value);

        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        List<String> marketArea = new ArrayList<String>();

        switch (getIntent().getStringExtra("TEXT")) {
            // case statements
            // values must be of same type of expression
            case "BENGALURU":
                marketArea.add("BANGALORE");
                break;
            case "CHITRADURGA":
                marketArea.add("Chitradurga");
                break;
            case "DAVANAGERE":
                marketArea.add("Channagiri");
                break;
            case "HAVERI":
                marketArea.add("Haveri");
                break;
            case "SHIVAMOGGA":
                marketArea.add("SHIVAMOGGA");
                marketArea.add("Bhadravathi");
                marketArea.add("Tirthahalli");
                marketArea.add("Sagara");
                marketArea.add("Hosanagar");
                marketArea.add("Shikaripura");
                break;
            case "UDUPI":
                marketArea.add("Kundapura");
                marketArea.add("Karkala");
                marketArea.add("Siddapura");
                break;
            case "TUMAKURU":
                marketArea.add("Tumakuru");
                marketArea.add("Huliyar");
                break;
            case "UTTARA KANNADA":
                marketArea.add("Kumata");
                marketArea.add("Sirsi");
                marketArea.add("Honnavara");
                marketArea.add("Yellapura");
                break;
            case "DAKSHINA KANNADA":
                marketArea.add("Mangaluru");
                marketArea.add("Puttur");
                marketArea.add("Bantwala");
                marketArea.add("Sulya");
                break;
            case "CHIKKAMAGALURU":
                marketArea.add("Mudigere");
                break;

        }



        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, marketArea);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);


        dateView = (TextView) findViewById(R.id.button1);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        date = showDate(year, month + 1, day);


    }
       // ((Button) findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
         //  @Override
           // public void onClick(View v) {
        private void onOpen(String item, StringBuilder date) {
            DatabaseHelper myDbHelper = new DatabaseHelper(MainActivityMarketArea.this);

            try {
                myDbHelper.createDataBase();
            } catch (IOException ioe) {
                throw new Error("Unable to create database");
            }
            try {
                myDbHelper.openDataBase();
            } catch (SQLException sqle) {
                throw sqle;
            }
            Toast.makeText(MainActivityMarketArea.this, "Successfully Imported", Toast.LENGTH_SHORT).show();
            // c = myDbHelper.query("arecanew1", null, null, null, null, null, null);
            c = myDbHelper.getCityAndDateRows("SELECT variety, max,min,modal,arrivals,unit FROM arecanew1 WHERE market='" + item + "' AND date='" + date.toString() + "'");
            StringBuilder sb = new StringBuilder();
            if (c.moveToFirst()) {
                do {
                        /*Toast.makeText(MainActivityMarketArea.this,
                                "_id: " + c.getString(0) + "\n" +
                                        "DATE: " + c.getString(1) + "\n" +
                                        "TIME: " + c.getString(2) + "\n" +
                                        "HEIGHT:  " + c.getString(3),
                                Toast.LENGTH_LONG).show();*/


                            /*String market = c.getString(0);
                            String date = c.getString(1);
                            String grade =  c.getString(2);
                            String arrivals =  c.getString(3);
                            String unit =  c.getString(4);
                            String min =  c.getString(5);
                            String max =  c.getString(6);
                            String modal =  c.getString(7);
                            String variety =  c.getString(8);
                            System.out.println("variety"+ variety);
                            sb.append(market).append(";").append(date).append(";").append(grade).append(";").append(arrivals).append(";").append(unit).append(";").append(min).append(";").append(max).append(";").append(modal).append("_");*/

                    //String market = c.getString(0);
                    //String date = c.getString(1);
                    //String grade =  c.getString(2);
                    String variety = c.getString(0);
                    String max = c.getString(1);
                    String min = c.getString(2);
                    String modal = c.getString(3);
                    String arrivals = c.getString(4);
                    String unit = c.getString(5);


                    sb.append(variety).append(";").append(max).append(";").append(min).append(";").append(modal).append(";").append(arrivals).append(";").append(unit).append("_");


                } while (c.moveToNext());
                System.out.println("sb.toString()" + sb.toString());
                System.out.flush();


            }
            String st = new String(sb);
            Log.e("MainArun", st);
            String[] rows = st.split("_");
                /*TableLayout tableLayout = (TableLayout)findViewById(R.id.tab);
                tableLayout.removeAllViews();


                for(int i=0;i<rows.length;i++) {
                    String row = rows[i];
                    TableRow tableRow = new TableRow(getApplicationContext());
                   // TableRow tableRow =  (TableRow)findViewById(R.id.tab1);
                    final String[] cols = row.split(";");

                    Handler handler = null;

                    for (int j = 0; j < cols.length; j++) {

                        final String col = cols[j];
                        final TextView columsView = new TextView(getApplicationContext());
                        columsView.setText(String.format("%7s", col));
                        tableRow.addView(columsView);
                    }
                }*/

            //TableLayout.LayoutParams tableLayoutParams = new
            //      TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT);
            //TableLayout table;
            if (rows[0].isEmpty() && rows.length == 1) {
                /*table = (TableLayout) findViewById(R.id.tab);
                table.removeAllViews();*/
                ListView lst = (ListView) findViewById(R.id.marketDailyRates);
                lst.removeAllViewsInLayout();
                Toast.makeText(getApplicationContext(), "No Data available for this day(market was closed)", Toast.LENGTH_LONG).show();
            } else {


                lst = (ListView) findViewById(R.id.marketDailyRates);
                lst.removeAllViewsInLayout();
                // CustomListViewRate customListViewRate;
                String[] row11;
                String row1;
                lst = (ListView) findViewById(R.id.marketDailyRates);
                List<String[]> rates = new ArrayList<String[]>();
                for (int i = 0; i < rows.length; i++) {
                     row1 = rows[i];
                     row11 = row1.split(";");

                    //String[] rates = {"Saraku", "100", "100","100","100","100"};
                    rates.add(row11);
                }
                customListViewRate = new CustomListViewRate(this, rates);

                lst.setAdapter(customListViewRate);




            }
        }



    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override


                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                    /*dateView.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                       */     // TODO Auto-generated method stub
                            onOpen(item,date);
                       /* }
                    });*/
                }
            };

    private StringBuilder showDate(int year, int month, int day) {

        //stringBuilder = new StringBuilder().append(day).append("-").append("Aug").append("-").append("18");
        if(month < 10) {
            date = new StringBuilder().append(day).append("/0").append(month).append("/").append(year);
        }
        else{
            date = new StringBuilder().append(day).append("/").append(month).append("/").append(year);
        }
        dateView.setText(date);


        return date;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
         item = parent.getItemAtPosition(position).toString();
         date = showDate(year, month + 1, day);
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
        onOpen(item,date);
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }

}
