package com.appbiz.arun.learningapp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity {

    String msg = "Android : ";

    /** Called when the activity is first created. */
    String url = "https://www.krishimaratavahini.kar.nic.in/MainPage/DailyMrktPriceRep2.aspx?Rep=Com&CommCode=140&VarCode=1&Date=15/05/2018&CommName=Arecanut%20/%20%E0%B2%85%E0%B2%A1%E0%B2%BF%E0%B2%95%E0%B3%86&VarName=Red%20/%20%E0%B2%95%E0%B3%86%E0%B2%82%E0%B2%AA%E0%B3%81";
    ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(msg, "The onCreate() event");


        Button titlebutton = (Button) findViewById(R.id.titlebutton);

        // Capture button click
        titlebutton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                // Execute Title AsyncTask
                new Title().execute();
            }
        });
    }


   /* public void onClickAddName(View view) {
        // Add a new student record
        ContentValues values = new ContentValues();
        values.put(StudentsProvider.NAME,
                ((EditText)findViewById(R.id.editText2)).getText().toString());

        values.put(StudentsProvider.GRADE,
                ((EditText)findViewById(R.id.editText3)).getText().toString());

        Uri uri = getContentResolver().insert(
                StudentsProvider.CONTENT_URI, values);

        Toast.makeText(getBaseContext(),
                uri.toString(), Toast.LENGTH_LONG).show();
    }*/
    /*public void onClickRetrieveStudents(View view) {
        // Retrieve student records
        String URL = "content://com.appbiz.arun.learningapp.StudentsProvider";

        Uri students = Uri.parse(URL);
        Cursor c = managedQuery(students, null, null, null, "name");

        if (c.moveToFirst()) {
            do{
                Toast.makeText(this,
                        c.getString(c.getColumnIndex(StudentsProvider._ID)) +
                                ", " +  c.getString(c.getColumnIndex( StudentsProvider.NAME)) +
                                ", " + c.getString(c.getColumnIndex( StudentsProvider.GRADE)),
                        Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }
    }

*/


   /* public void DisplayWebPageDomValues(View view){

        try {
            Document doc = Jsoup.connect("https://www.krishimaratavahini.kar.nic.in/MainPage/DailyMrktPriceRep2.aspx?Rep=Com&CommCode=140&VarCode=3&Date=19/05/2018&CommName=Arecanut%20/%20%E0%B2%85%E0%B2%A1%E0%B2%BF%E0%B2%95%E0%B3%86&VarName=Sippegotu%20/%20%E0%B2%B8%E0%B2%BF%E0%B2%AA%E0%B3%8D%E0%B2%AA%E0%B3%86%E0%B2%97%E0%B3%8B%E0%B2%9F%E0%B3%81").get();
        }
        catch (IOException ex){
            ex.getMessage();
        }


    }*/

    // Title AsyncTask
    public class Title extends AsyncTask<Void, String, Void> {
        public String title;
        List<String> data;
       /* @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setTitle("Android Basic JSoup Tutorial");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }*/

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // Connect to the web site
                Document document = Jsoup.connect(url).get();
                System.out.println("Arun G s");
                // Get the html document title
                // Elements title1 = document.select("#_ctl0_content5_Table1 > tbody > tr:nth-child(1)");

                // String title = title1.attr(arg0)
                // System.out.println("output" + title);

                Element table = document.select("table").get(1);
                Elements rows = table.select("tr");


                System.out.println("rows.get(0).text()" + rows.get(0).text());

                title = " ";
                for(int i = 1; i< rows.size() ; i++) {
                    if(!rows.get(i).text().isEmpty()) {

                          title = title.concat(rows.get(i).text().concat("-"));
                          System.out.println("title" + title);
                    }
                    else {
                        break;
                    }
                }




            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("e.printStackTrace()"+ e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set title into TextView
           //TextView txttitle = (TextView) findViewById(R.id.titletxt);
            //txttitle.setText(title);
            TableLayout.LayoutParams tableLayoutParams = new
                    TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,  TableLayout.LayoutParams.WRAP_CONTENT);
            TableLayout table;

            table = findViewById(R.id.mytable);
            table.setBackgroundColor(Color.BLACK);
            //table =new TableLayout(com.appbiz.arun.learningapp.MainActivity.this);
            /*for(int i=0;i<data.size();i++)
            {*/

            TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams(
                    android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                    android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
            tableRowParams.setMargins(1, 1, 1, 1);


            TableRow row =new TableRow(com.appbiz.arun.learningapp.MainActivity.this);
            row.setGravity(Gravity.CENTER_VERTICAL);
            row.setPadding(1, 1, 1, 1);
            row.setBackgroundColor(Color.WHITE);

            String [] title1 = title.split("-");
            for(int j=0; j < title1.length; j++) {

                if((title1[j].trim().contains("Hosa")) ){

                    title1[j] = title1[j].trim().replace("Hosa Chali","Hosa_Chali");

                }
                else if((title1[j].trim().contains("Hale"))){
                    title1[j] =  title1[j].trim().replace("Hale Chali","Hale_Chali");
                }
                else if((title1[j].trim().contains("New"))){
                    title1[j] =  title1[j].trim().replace("New Variety","New_Variety");
                }
                else if((title1[j].trim().contains("Old"))){
                    title1[j] =   title1[j].trim().replace("Old Variety","Old_Variety");
                }
                String[] title2 = title1[j].trim().split(" ");
                data = Arrays.asList(title2);


                DatabaseHelper myDbHelper = new DatabaseHelper(MainActivity.this);

                try {
                    myDbHelper.createDataBase();
                } catch (IOException ioe) {
                    throw new Error("Unable to create database");
                }
                try {
                    myDbHelper.openDataBase();
                } catch (SQLException sqle) {
                    throw sqle;
                }
                Toast.makeText(MainActivity.this, "Successfully Imported", Toast.LENGTH_SHORT).show();
                // c = myDbHelper.query("arecanew1", null, null, null, null, null, null);
                try {
                    myDbHelper.insertProdData("INSERT INTO arecanew1 (market,date,grade,arrivals,unit,min,max,modal,variety) VALUES ('"+data.get(0)+"','"+data.get(1)+"','AVERAGE','"+data.get(3)+"','Quintal','"+data.get(4)+"','"+data.get(5)+"','"+data.get(6)+"','"+data.get(2)+"')");

                } catch (SQLException sqle){

                    sqle.printStackTrace();
                }
                StringBuilder sb = new StringBuilder();






            //TableRow row = findViewById(R.id.mytable1);
            /*String phone0 = data.get(0);
            String phone1 = data.get(1);
            String phone2 = data.get(2);
            String phone3 = data.get(3);
            String phone4 = data.get(4);
            String phone5 = data.get(5);
            String phone6 = data.get(6);

            TextView tv0=new TextView(com.appbiz.arun.learningapp.MainActivity.this);
            TextView tv1=new TextView(com.appbiz.arun.learningapp.MainActivity.this);
            TextView tv2=new TextView(com.appbiz.arun.learningapp.MainActivity.this);
            TextView tv3=new TextView(com.appbiz.arun.learningapp.MainActivity.this);
            TextView tv4=new TextView(com.appbiz.arun.learningapp.MainActivity.this);
            TextView tv5=new TextView(com.appbiz.arun.learningapp.MainActivity.this);
            TextView tv6=new TextView(com.appbiz.arun.learningapp.MainActivity.this);

            tv0.setTextSize(10);
            tv1.setTextSize(10);
            tv2.setTextSize(10);
            tv3.setTextSize(10);
            tv4.setTextSize(10);
            tv5.setTextSize(10);
            tv6.setTextSize(10);

            tv0.setTextColor(Color.BLUE);
            tv1.setTextColor(Color.BLUE);
            tv2.setTextColor(Color.BLUE);
            tv3.setTextColor(Color.BLUE);
            tv4.setTextColor(Color.BLUE);
            tv5.setTextColor(Color.BLUE);
            tv6.setTextColor(Color.BLUE);

            tv0.setGravity(Gravity.CENTER);
            tv1.setGravity(Gravity.CENTER);
            tv2.setGravity(Gravity.CENTER);
            tv3.setGravity(Gravity.CENTER);
            tv4.setGravity(Gravity.CENTER);
            tv5.setGravity(Gravity.CENTER);
            tv6.setGravity(Gravity.CENTER);

            tv0.setText(phone0);
            tv1.setText(phone1);
            tv2.setText(phone2);
            tv3.setText(phone3);
            tv4.setText(phone4);
            tv5.setText(phone5);
            tv6.setText(phone6);

            row.addView(tv0);
            row.addView(tv1);
            row.addView(tv2);
            row.addView(tv3);
            row.addView(tv4);
            row.addView(tv5);
            row.addView(tv6);

            table.addView(row);*/
            //row =new TableRow(com.appbiz.arun.learningapp.MainActivity.this);
            //}
            }
           // mProgressDialog.dismiss();
        }
    }





}
