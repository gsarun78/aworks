package com.appbiz.arun.learningapp;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import static android.content.ContentValues.TAG;

public class DBHelper extends SQLiteOpenHelper {

    private final Context myContext;
    String DB_PATH = null;
    private SQLiteDatabase db;
    public static final String DATABASE_NAME = "arecamission.sqlite";
    public static final String RATES_TABLE_NAME = "arecanew1";
    //public static final String RATES_COLUMN_ID = "id";
    public static final String RATES_COLUMN_MARKET = "market";
    public static final String RATES_COLUMN_DATE = "date";
    public static final String RATES_COLUMN_GRADE = "grade";
    public static final String RATES_COLUMN_ARRIVALS = "arrivals";
    public static final String RATES_COLUMN_UNITS = "units";
    public static final String RATES_COLUMN_MIN = "min";
    public static final String RATES_COLUMN_MAX = "max";
    public static final String RATES_COLUMN_MODAL = "modal";

    static final String CREATE_DB_TABLE =
            " CREATE TABLE " + RATES_TABLE_NAME +"(market VARCHAR NOT NULL , date DATETIME NOT NULL , grade VARCHAR NOT NULL , arrivals INTEGER NOT NULL , unit VARCHAR NOT NULL , min INTEGER NOT NULL , max INTEGER NOT NULL , modal INTEGER NOT NULL );";

    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
        this.myContext = context;


    }



    public Cursor getData() {

        File file = new File("/data/data/com.appbiz.arun.learningapp/databases/arecamission.sqlite");
        if (file.exists()) {
            boolean result = file.setExecutable(true);
            Log.e(TAG, "trpb67, RESULT IS " + result);

        }



        Cursor res =  db.rawQuery( "select * from arecanew1",null );
        return res;
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       sqLiteDatabase.execSQL(CREATE_DB_TABLE);
        File file = new File("/data/data/com.appbiz.arun.learningapp/databases/arecamission.sqlite");
        if (file.exists()) {
            boolean result = file.setExecutable(true);
            Log.e(TAG, "trpb67, RESULT1 IS " + result);

        }
        Log.d("messageoncreate","hi arun in oncreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " +  RATES_TABLE_NAME);
        onCreate(sqLiteDatabase);
        Log.d("messageonupgrade","hi arun in onUpgrade");
    }
 }
