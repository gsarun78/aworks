package com.appbiz.arun.learningapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewAnimator;

public class CustomListView extends ArrayAdapter<String> {


    private String[] cityname;
    private Integer[] imgid;
    private Main2Activity context;

    public CustomListView(Main2Activity context, String[] cityname, Integer[] imgid) {
        super(context, R.layout.layout_city, cityname);

        this.context = context;
        this.cityname = cityname;
        this.imgid = imgid;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View r = convertView;
        ViewHolder viewHolder = null;
        if(r==null){

            LayoutInflater layoutInflater = context.getLayoutInflater();
            r=layoutInflater.inflate(R.layout.layout_city,null,true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);

        }
        else{
            viewHolder =(ViewHolder) r.getTag();

        }

        viewHolder.ivw.setImageResource(imgid[position]);
        viewHolder.tvw1.setText(cityname[position]);



        return r;
    }



    class ViewHolder {

        TextView tvw1;
        ImageView ivw;

        ViewHolder(View v) {

            tvw1 = (TextView) v.findViewById(R.id.textView4);
            ivw = (ImageView) v.findViewById(R.id.imageViewCity);
        }

    }
}
