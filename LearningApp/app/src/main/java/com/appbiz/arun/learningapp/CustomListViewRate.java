package com.appbiz.arun.learningapp;

import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewRate  extends ArrayAdapter<String> {

    private List rates;
    //private String[] rows;
    //private MainActivityMarketArea context;
    private  MainActivityMarketArea context;


    public CustomListViewRate(MainActivityMarketArea context, List rates) {
        super(context, R.layout.layout_daily_market_rate,rates);

        this.context = context;
        this.rates = rates;

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View r = convertView;
        ViewHolder viewHolder = null;
        //String a = getItem(position);
        //System.out.println("arun"+a);
        //CustomListViewRate customListViewRate = getItem(position);
        //CustomListViewRate rowss = getItem(position);
       /* for (int i = 0; i < rows.length; i++) {
            String row1 = rows[i];
            String[] rates = row1.split(";");*/
        if(r==null){

            LayoutInflater layoutInflater = context.getLayoutInflater();
            r=layoutInflater.inflate(R.layout.layout_daily_market_rate,null,true);
            viewHolder = new CustomListViewRate.ViewHolder(r);
            r.setTag(viewHolder);
            //r.setTag(viewHolder);
            //r.setSaveEnabled(true);


        }
        else{

            viewHolder =(CustomListViewRate.ViewHolder) r.getTag();


        }
        String [] raterow = (String [])rates.get(position);
        viewHolder.tvw1.setText(raterow[0]);
        viewHolder.tvw2.setText(raterow[1]);
        viewHolder.tvw3.setText(raterow[2]);
        viewHolder.tvw4.setText(raterow[3]);
        viewHolder.tvw5.setText(raterow[4]);
        viewHolder.tvw6.setText(raterow[5]);




        //}

        return r;
    }



    class ViewHolder {

        TextView tvw1;
        TextView tvw2;
        TextView tvw3;
        TextView tvw4;
        TextView tvw5;
        TextView tvw6;



        ViewHolder(View v) {
            //for (int i = 0; i < rows.length; i++) {
                tvw1 = (TextView) v.findViewById(R.id.textView15);
                tvw2 = (TextView) v.findViewById(R.id.textView14);
               tvw3 = (TextView) v.findViewById(R.id.textView6);
                tvw4 = (TextView) v.findViewById(R.id.textView7);
                tvw5 = (TextView) v.findViewById(R.id.textView8);
                tvw6 = (TextView) v.findViewById(R.id.textView9);
            //}

        }

    }
}